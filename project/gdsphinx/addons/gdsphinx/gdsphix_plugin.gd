@tool
extends EditorPlugin


# A class member to hold the dock during the plugin life cycle.
var edui
var scriptui


func _enter_tree():
	# Initialization of the plugin goes here.
	# Load the dock scene and instance it.
	edui = get_editor_interface()
	scriptui = edui.get_script_editor()
	#scriptui.get_children()[0][0]
	var sui_children = scriptui.get_children()
	var vbcont = sui_children[0]
	var vbc = vbcont.get_children()
	var smenus = vbc[0]
	var smc = smenus.get_children()
	var fmenu_popup = smc[0].get_popup() 
	var fmenu_item_count = fmenu_popup.get_item_count() 
	var fmenu_first =  fmenu_popup.get_item_text(0)
	var outp = fmenu_first
	var gdsphinx_menu_button = MenuButton.new()
	gdsphinx_menu_button.text = "GDSphinx"
	
	gdsphinx_menu_button.get_popup().add_item("Generate Documentation")
	gdsphinx_menu_button.get_popup().connect("id_pressed", build_doc)
	smenus.add_child(gdsphinx_menu_button)
	smenus.move_child(gdsphinx_menu_button, 5)
	print(smc[0])

	# Add the loaded scene to the docks.
	
	# Note that LEFT_UL means the left of the editor, upper-left dock.


func _exit_tree():
	# Clean-up of the plugin goes here.
	# Remove the dock.
	edui = get_editor_interface()
	scriptui = edui.get_script_editor()
	#scriptui.get_children()[0][0]
	var sui_children = scriptui.get_children()
	var vbcont = sui_children[0]
	var vbc = vbcont.get_children()
	var smenus = vbc[0]
	var smc = smenus.get_children()
	var fmenu_popup = smc[0].get_popup() 
	var fmenu_item_count = fmenu_popup.get_item_count() 
	var fmenu_first =  fmenu_popup.get_item_text(0)
	# Erase the control from the memory.
	smenus.remove_child(smenus.get_child(5))

func build_doc(id_pressed):
	var output = []
	if id_pressed==0:
		print("the user clicked the generate doc button")
		OS.execute("bash",["./addons/gdsphinx/docs/build_help.sh"], output, true )
		for line in output:
			print( line )
	else:
		print("some other menu items was chosen instead")
