#!/bin/bash


build_help()
{
    echo "build.sh [options]"
    echo "          -b build project"
    echo "          -bf build full project including -cpython and -cpyenv"
    echo "          -h display this help message"
    echo "          -c copy python files"
    echo "          -epy execute python"
    echo "          -cpython build cpython"
    echo "          -cpyenv build the venv for our custom cpython"
}

build()
{
  rm -rf build
  mkdir build
  mkdir ./build/gdsphinx
  cp -r ./cpython_venv ./build/gdsphinx
  mv ./build/gdsphinx/cpython_venv ./build/gdsphinx/python
  cp -r ./project/gdsphinx/addons/gdsphinx/*.* ./build/gdsphinx
  mkdir ./build/gdsphinx/docs
  cp ./build_help.sh ./build/gdsphinx/docs/build_help.sh
  cd ./build/gdsphinx/docs
  export LD_LIBRARY_PATH=../python/lib
  ../python/bin/python ../python/bin/sphinx-quickstart
  
}

build_full()
{
  build
}
cpython_build(){
  echo "building cpython modules ..."
  rm -rf cpython_build
  cd cpython
  git pull
  make clean
  ./configure --prefix=$HOME/coding/gdsphinx/cpython_build --enable-shared LDFLAGS=-Wl,-rpath=$HOME/gdsphinx/cpython_build/lib --enable-optimizations --enable-shared
  make -s -j20
  make install
  echo ".. finished building cython modules"
}

execute_python()
{
  export LD_LIBRARY_PATH=$HOME/coding/gdsphinx/cpython_build/lib
  source ./cpython_venv/bin/activate
  python
}

cpyenv_build(){
  echo "building cpython env ..."
  rm -rf cpython_venv
  cd ./cpython_build
  export LD_LIBRARY_PATH=$HOME/coding/gdsphinx/cpython_build/lib
  ./bin/python3 -m venv ../cpython_venv
  cd ..
  cp -r ./cpython_build/lib/python3.11 ./cpython_venv/lib/python3.11
  mv -f ./cpython_venv/lib/python3.11/python3.11  ./cpython_venv/lib/python3.11/pystdlib
  rm ./cpython_venv/bin/python
  cp -rf ./cpython_build/bin/python3.11 ./cpython_venv/bin/python
  cp -r ./cpython_build/lib/libpython3.11.so.1.0 ./cpython_venv/lib
  echo "... finished building cpython env"
  echo  "installing third party libraries ..."
  source ./cpython_venv/bin/activate
  pip install sphinx
  deactivate
  echo "... finished installing third libraries" 
}


if [ "$1" == "-h" ]
then
  build_help
elif [ "$1" == "-cpython" ]
then
  cpython_build
elif [ "$1" == "-cpyenv" ]
then
  cpyenv_build
elif [ "$1" == "-epy" ]
then
  execute_python
elif [ "$1" == "-b" ]
then
  build
else
  build_help
fi

